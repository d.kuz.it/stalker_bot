module.exports = {
    enterZone : {
            reply_markup: JSON.stringify({
                inline_keyboard: [
                    [{text: 'Пересечь колючку', callback_data: '/enter'}]
                ]
            })
    },
    ChooseRoad : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Пойти по дороге', callback_data: '/1'}],[{text: 'Пойти в обход дороги', callback_data: '/2'}]
            ]
        })
    },

    KPP : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Начать ругаться ', callback_data: '/3'}],[{text: 'Договорится', callback_data: '/4'}]
            ]
        })
    },
    KPP1 : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Пойти в деревню', callback_data: '/KPP'}]
            ]
        })
    },
    village : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Завязать диалоги', callback_data: '/5'}],[{text: 'Агресивно послать их ', callback_data: '/6'}]
            ]
        })
    },
    sidor: {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Спуститься', callback_data: '/7'}]
            ]
        })
    },
    trade : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Взять в долг ', callback_data: '/8'}],
                [{text: 'выполнить задание ', callback_data: '/9'}]
            ]
        })
    },
    VoiceInHead : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Поговорить с волком ', callback_data: '/10'}]
            ]
        })
    },
    wolf: {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Волк, сам занимайся этим', callback_data: '/11'}],
                [{text: 'Принять задание волка', callback_data: '/12'}],
            ]
        })
    },
    wolf2 : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Присесть на пенек возле туннеля', callback_data: '/13'}]
            ]
        })
    },
    endgame : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Начать заного', callback_data: '/start'}]
            ]
        })
    },
    mission1 : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Начать разведку', callback_data: '/14'}]
            ]
        })
    },
    mission2 : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Убежать что есть мочи ', callback_data: '/15'}],
                [{text: 'Начать стрелять в туши ', callback_data: '/16'}],

            ]
        })
    },
    mission3 : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Доложить о разведке ', callback_data: '/17'}],
            ]
        })
    },
    mission4 : {
        reply_markup: JSON.stringify({
            inline_keyboard: [
                [{text: 'Вернуть долг торговцу  ', callback_data: '/18'}],
            ]
        })
    }
}