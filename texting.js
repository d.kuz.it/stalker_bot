module.exports = {
    died: 'Вы были убиты и выброшены на сьедение собакам',
    intro: 'Вот ты отбросил все сомнения и наконец оказался тут перед входом в совершенно новый мир о котором ты слышал из новостей и уст других людей, отличный способ заработать или найти свою смерть, кто знает что ждет нас там. Остался буквально последний рывок и я внутри',
    enter: 'Проползая под колючей проволкой вы изорвали куртку в которой были и промокли от влажной травы, затем пройдя через небольшой неиспользуемый КПП вы стали на разпутье, с одной стороны можно было пойти дальше по дороге , а с другой пойти по неизвестной тропинке в обход потрескавшейся асфальтной дороги',
    KppText: 'Вы испугавшись заблудиться в этом новом месте решаете пойти по дороге , и буквально через пару минут оказываетесь на открытой дороге где вас и замечает патруль . Арестовав вас они отвели вас в в подсобку и начали задавать разные вопросы кто и откуда вы тут взялись на охраняемой територии, так как я был безоружный то они не особо наседали на меня и дали право слова',
    KppText1: 'Вы рассказали историю о себе, о своей семье, и о цели для которой вы здесь, хотя они не хотели вас пропускать  все же обьяснили где тут ближайшие лди и отправили восвояси, надеюсь больше их не увижу ',
    villageText: 'Пройдя около километра по тропинке вы начали видеть дым от костров за деревьямиб подойдя поближе вы увидели как группа людей сидят возле костра и что то обсуждают. Вы поняли что пришли в деревню неопытных сталкеров, о которой слышали по наслышке от  других людей, к вам сразу начали приближаться вооруженые люди только завидев вас',
    villageText1: 'После вашей истории сталкера убедились что вы нормальный мужик и пригласисли к себе за костер, угостили колбасой и консервой и рассказали в друх словвах что тут да как, сказали что без снаряги я труп и отправили к местному торговцу Сидоровичу',
    sidorText: 'Что ж сталкер добро пожаловать я тут местный торговец и до самого бара другого ты не найдешь , вижу что пришел ты совсем пустой ко мне , но у меня как раз есть набор новичка с ружьем курткой и фонариком, ну и мешочек болтов проверять аномалии. Отдам весь набор за тыщу',
    VoiceInHead1: 'Вы успешно Раздобыли снаряжение, настало время рассчитаться за него ',
    wolfText: 'Волк уже слышал про вас от других сталкеров и готов выдать вам задание если вас интересует способ немного заработать ',
    wolfText2: 'Сталкеры решили что вы что то мутите раз отказались от задания Волка и выгнали вас из лагеря',
    endgameText: 'Вскоре за пределами лагеря вас нашли люди Cидоровича , забрали ваше сняржение и бросили умирать, хотя они вас не трогали шансов у вас не много ',
    brif1: 'Сталкеры видя ваш энтузиаст вызвались вам помочь ' +
        'Ваше первое задание было провести разведку территории вокруг лагеря ',
    brif2: 'Во время разведки вы наткнулись на стаю собак, вы и ваш напарник немного расстерялись , ваши действия?',
    leave: 'Вы убежали бросив сталкера на смерть, но он выжил и в лагере все узнали что произошло и выгнали вас ',
    shootText: 'Вы выстрелили дробью по собаке и она упала, остальные убежали сами от выстрела и страха ',
    shootText2: 'Вы доложили волку о разведке, и он дал вам денег покрыть расходы у Cидоровича' +
        'Так же спасенный вами сталкер пообещал найти способ отблагодарить вас'
    ,
    tradeText2: 'Отдав долг Cидору перед вами открылись перспективы и понимания о том что происходит в зоне, тут не место для прогулок, а опасные места , ' +
        'хоть вы справились с заданием и раздобыли оружие все только начинается '

}
