require('dotenv').config()

const TelegramApi = require('node-telegram-bot-api');
const token = process.env.TOKEN
const {enterZone, ChooseRoad, KPP, village, sidor, trade, KPP1, VoiceInHead, wolf, wolf2, endgame, mission1, mission2,
    mission3, mission4
} = require('./buttons')
const {intro, enter, KppText, villageText, villageText1, died, KppText1, sidorText, two1,
    VoiceInHead1, wolfText, wolfText2, endgameText, brif1, brif2, leave, shootText, shootText2, tradeText2
} = require("./texting");

const bot = new TelegramApi(token, {polling: true})
const chats = {}




const start =  async ()=> {
   await bot.setMyCommands([
        {command: '/start', description: 'Приветствие' },
       {command: '/info', description: 'Описание' },

   ])

    bot.on('message', async msg => {
        const text = msg.text
        const chatId = msg.chat.id
        if(text === '/start') {
            await  bot.sendMessage(chatId, intro, enterZone)
        }
        if(text === '/info') {
                await  bot.sendMessage(chatId, 'Это игра в сталкер в которой тебе надо дойти до северной дороги, и постараться вернуться обратно с наградой)')
            }
    bot.on('callback_query',  async msg=>{
        const chatId = msg.message.chat.id;
        const data = msg.data;
        if(data === '/enter'){
            return  bot.sendMessage(chatId, enter, ChooseRoad)
        }
        //Кпп часть
        if(data === '/1') {
            return bot.sendMessage(chatId, KppText, KPP)
        }
            if(data === '/3'){
                return  bot.sendMessage(chatId,died)
            }
            if(data === '/4'){
                return  bot.sendMessage(chatId,KppText1,KPP1)
            }
        //деревня часть
        if(data === '/KPP'){
            return bot.sendMessage(chatId, villageText, village)
        }
        if(data === '/2') {
            return bot.sendMessage(chatId, villageText, village)
        }
            if(data === '/5'){
                return  bot.sendMessage(chatId, villageText1 , sidor)
            }
            if(data === '/6'){
                return  bot.sendMessage(chatId,died )
            }
         //спуск к торговцу
        if(data === '/7'){
            return  bot.sendMessage(chatId,sidorText,trade )
        }
        if(data === '/8' || data === '/9'){
            return  bot.sendMessage(chatId,VoiceInHead1,VoiceInHead )
        }
        if(data === '/10'){
            return  bot.sendMessage(chatId,wolfText,wolf )
        }
        if(data === '/11'){
            return  bot.sendMessage(chatId,wolfText2, wolf2)
        }
        if(data === '/13'){
            await  bot.sendMessage(chatId,endgameText, endgame )
        }
        if(data === '/start') {
            await  bot.sendMessage(chatId, intro, enterZone)
        }
        if(data === '/12') {
            await  bot.sendMessage(chatId, brif1, mission1)
        }
        if(data === '/14') {
            await  bot.sendMessage(chatId, brif2, mission2)
        }
        if(data === '/15'){
            await  bot.sendMessage(chatId, leave, wolf2)
        }
        if(data === '/13'){
            await  bot.sendMessage(chatId,endgameText, endgame )
        }
        if(data === '/16'){
            await  bot.sendMessage(chatId,shootText, mission3 )
        }
        if(data === '/17'){
            await  bot.sendMessage(chatId,shootText2, mission4 )
        }
        if (data === '/18'){
            await  bot.sendMessage(chatId,tradeText2,endgame )
        }
    } )
}
)
}
start()

